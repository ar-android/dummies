<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class HotelController extends Controller
{
    
    public function index(Request $request)
    {
    	if ($request->isMethod('post')) {
            $city = array('Ambon', 'Bandar Lampung', 'Jakarta', 'Yogyakarta', 'Surabaya', 'Sumatra Barat', 'Sulawesi');
    		$hotel = array('Novotel', 'Amelia', 'Hotel Maharani', 'Hotel POP', 'Hotel Kaisa', 'Sumatra Barat', 'Maha Radja');
    		$data = array();

    		for ($i=0; $i < count($hotel); $i++) { 
    			$data[] = array(
                    'city' => $city[$i],
                    'hotel' => $hotel[$i]
                    );
    		}

    		return response($data);
    	}else{
            $res['success'] = false;
            $res['message'] = 'This is for post method!';
            
            return response($res);
        }
    }
}
