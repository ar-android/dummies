<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Inspections;
use Image;

class InspectionController extends Controller
{

    /**
     * Insection routes index
     */
    public function index()
    {
    	$data = Inspections::where('approved', 0)->take(5)->get();

    	return view('home.inspection_list', ['inspection' => $data]);
    }

    /**
     * Create new inspections
     */
    public function create()
    {
    	return view('home.create_inspection');
    }

    /**
     * Post inspections
     */
    public function post(Request $request)
    {
    	$title = $request->input('title');
    	$company = $request->input('company');
    	$description = $request->input('description');
		$pictures = $request->file('pictures');
		$filename = time() . '.' . $pictures->getClientOriginalExtension();
    	Image::make($pictures)->resize(450, 300)->save(public_path('uploads/inspections/'. $filename));

    	$insert = Inspections::create([
    		'title' => $title,
    		'picture' => $filename,
    		'company' => $company,
    		'description' => $description,
    		'approved' => false
    	]);

    	if ($insert) {
    		return redirect()->to('/inspections');
    	}else{
    		$res['success'] = false;
    		$res['message'] = 'Cannot input to database';
    		
    		return response($res);
    	}
    }

    /**
     * Get data inspections
     */
    public function api_get()
    {
    	$inspections = Inspections::where('approved',0)->take(5)->get();
    	if (count($inspections) < 1) {
    		$res['success'] = false;
    		$res['message'] = 'No data!';
    		
    		return response($res);
    	}else{
	    	return response($inspections);		
    	}
    }

    /**
     * Get data inspections
     *
     * URL /api/inspections/new
     */
    public function api_create(Request $request)
    {

    	/**
    	 * Hadle post with api inspections
    	 */
    	
    	if ($request->hasFile('pictures')) {

	    	$title = $request->input('title');
	    	$company = $request->input('company');
	    	$description = $request->input('description');

	    	// Handle upload images
			$pictures = $request->file('pictures');
			$filename = time() . '.' . $pictures->getClientOriginalExtension();
	    	Image::make($pictures)->resize(450, 300)->save(public_path('uploads/inspections/'. $filename));

	    	// Post to databse
	    	$insert = Inspections::create([
			    		'title' => $title,
			    		'picture' => $filename,
			    		'company' => $company,
			    		'description' => $description,
			    		'approved' => false
			    		]);

	    	if ($insert) {
	    		$res['success'] = true;
	    		$res['message'] = 'Succes insert to database!';
	    		
	    		return response($res);
	    	}else{
	    		$res['success'] = false;
	    		$res['message'] = 'Cannot input to database';
	    		
	    		return response($res);
	    	}
    	}else {
    		$res['success'] = false;
    		$res['message'] = 'Nothing pictures!';
    		
    		return response($res);
    	}

    }

    /**
     * Delete inspections
     *     
     * @param  integer $id id data
     */
    public function deletes($id)
    {
        $delete = Inspections::destroy($id);
        if ($delete) {
            return redirect()->to('/inspections');
        }else{
            $res['success'] = false;
            $res['message'] = 'Failed delete';
            
            return response($res);
        }
    }
}
