<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inspections extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'picture', 'company', 'description', 'approved'
    ];

    /**
     * Format dates
     * @var array
     */
    protected $dates = ['dob'];
}
