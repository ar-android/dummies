@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Inspections</div>

                <div class="panel-body">
                    <form enctype="multipart/form-data" action="/inspections/post" method="post">

                        <div class="form-group">
                            <label >Title</label>
                            <input class="form-control" type="text" name="title" placeholder="Input title here">
                        </div>

                        <div class="form-group">
                            <label >Company</label>
                            <input class="form-control" type="text" name="company" placeholder="Input title here">
                        </div>

                        <div class="form-group">
                            <label >Descriptions</label>
                            <input class="form-control" type="text" name="description" placeholder="Input title here">
                        </div>

                        <div class="form-group">
                            <label >Pictures</label>
                            <input type="file" name="pictures">
                        </div>

                        <input type="submit" class="btn btn-primary " value="Send">
                        <a href="{{url('/inspections') }}" class="btn btn-primary">Cancel</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
