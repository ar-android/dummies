@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-0">
            <img src="/uploads/avatars/{{ $user->avatars }}" style="width:150px; height:150px; float:left; border-radius: 50%; margin-right: 25px ">
            <h2>{{$user->name}}'s Profile</h2>
            <form enctype="multipart/form-data" action="/profile" method="POST">
                <label>Update Profile Image</label>
                <input type="file" name="avatar">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <br/>
                <input type="submit" class="pull-left btn btn-sm btn-primary">
            </form>
        </div>
    </div>
</div>
@endsection
